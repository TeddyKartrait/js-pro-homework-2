let { saveList } = await import ("./todo-app/localStorage.js");
let { saveDo } = await import ("./todo-app/api.js");
let { changeDo } = await import ("./todo-app/api.js");
let { deleteDo } = await import ("./todo-app/api.js");

let listArray = [],
    owner,
    anchor = document.querySelector('.anchor');

function createAppTittle(title) {
        let appTitle = document.createElement('h2');
        appTitle.innerHTML = title;
        return appTitle;
}

function createTodoItemForm() {
    let form = document.createElement('form');
    let input = document.createElement('input');
    let buttonWrapper = document.createElement('div');
    let button = document.createElement('button');

    form.classList.add('input-group', 'mb-3');
    input.classList.add('form-control');
    input.placeholder = 'Введите название нового дела';
    buttonWrapper.classList.add('input-group-append');
    button.classList.add('btn', 'btn-primary');
    button.setAttribute("disabled", "disabled");
    button.textContent = 'Добавить дело';

    input.addEventListener('keydown', function time() {
        setTimeout(time, 1)
        if (input.value === "") {
            button.setAttribute("disabled", "disabled");
        } else {
            button.removeAttribute("disabled", "disabled");
        };
    })


    buttonWrapper.append(button);
    form.append(input);
    form.append(buttonWrapper);

    return {
        form,
        input,
        button,
    };
}

function createTodoList() {
    let list = document.createElement('ul');
    list.classList.add('list-group');
    return list;
}

function createTodoItem(obj) {

    let item = document.createElement('li');

    let buttonGroup = document.createElement('div');
    let doneButton = document.createElement('button');
    let deleteButton = document.createElement('button');


    item.classList.add('list-group-item', 'd-flex', 'justify-content-between', 'align-items-center');
    item.textContent = obj.name;

    buttonGroup.classList.add('btn-group', 'btn-group-sm');
    doneButton.classList.add('btn', 'btn-success')
    doneButton.textContent = 'Готово';
    deleteButton.classList.add('btn', 'btn-danger')
    deleteButton.textContent = 'Удалить';

    if (obj.done == true) item.classList.add('list-group-item-success')

    doneButton.addEventListener('click', function() {
        item.classList.toggle('list-group-item-success')

        listArray.forEach(listItem => {
            if(listItem.id == obj.id) listItem.done = !listItem.done
        })

        if (anchor.classList.contains('local-storage')) {
            saveList(listArray, owner)
        } else changeDo(obj)
    });

    deleteButton.addEventListener('click', function() {
        if (confirm('Вы уверены?')) {
            item.remove();
            let i = 0
            for (; i < listArray.length; i++) {
                if (listArray[i].id == obj.id) listArray.splice(i, 1)
            }

            if (anchor.classList.contains('local-storage')) {
                saveList(listArray, owner)
            } else deleteDo(obj)
        }
    });

    buttonGroup.append(doneButton);
    buttonGroup.append(deleteButton);
    item.append(buttonGroup);

    return {
        item,
        doneButton,
        deleteButton,
    }
}

function getNewID(arr) {
    let max = 0;
    arr.forEach(item => {
        if (item.id > max) max = item.id
    })
    return max + 1; 1
}

function createTodoApp(container, title = 'Список дел', arr, name) {
    let todoAppTitile = createAppTittle(title);
    let todoItemForm = createTodoItemForm();
    let todoList = createTodoList();

    if (localStorage.getItem('storage') == 'api') {
        anchor.classList.remove('local-storage')
        anchor.textContent = 'Перейти на локальное хранилище'
    } else anchor.textContent = 'Перейти на серверное хранилище'

    listArray = arr
    owner = name

    container.append(todoAppTitile);
    container.append(todoItemForm.form);
    container.append(todoList);

    listArray.forEach(itemList => {
        let todoItem = createTodoItem(itemList);
        todoList.append(todoItem.item);
    })

    todoItemForm.form.addEventListener('submit', function(e) {

        e.preventDefault();

        if (!todoItemForm.input.value) {
            return;
        }

        let newItem = {
            id: getNewID(listArray),
            name: todoItemForm.input.value,
            done: false
        }

        let todoItem = createTodoItem(newItem);

        listArray.push(newItem)

        if (anchor.classList.contains('local-storage')) {
            saveList(listArray, owner);
        } else saveDo(todoItemForm.input.value, owner)
        

        todoList.append(todoItem.item);

        todoItemForm.input.value = '';
    });
}

   export { createTodoApp };