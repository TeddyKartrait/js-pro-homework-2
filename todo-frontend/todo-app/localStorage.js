function saveList(arr, name) {
    localStorage.setItem(name, JSON.stringify(arr));
}

function storage(name, str) {
    localStorage.setItem(name, str)
}

function getArrLocal (str) {
    let localData = localStorage.getItem(str),
        listArray = []

    if (localData !== null && localData !== []) {
        listArray = JSON.parse(localData)
    }

    return listArray
}

export { storage };
export { saveList };
export { getArrLocal };
