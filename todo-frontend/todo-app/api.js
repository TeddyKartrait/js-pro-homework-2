async function saveDo (name, owner) {
    await fetch('http://localhost:3000/api/todos', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json'},
        body: JSON.stringify({
            name: name,
            owner: owner,
            done: false
        })
    });
}

async function changeDo (obj) {
    await fetch(`http://localhost:3000/api/todos/${obj.id}`, {
            method: 'PATCH',
            headers: { 'Content-Type': 'application/json'},
            body: JSON.stringify({
                name: obj.name,
                owner: obj.owner,
                done: obj.done
            })
        });
}

async function deleteDo (obj) {
    await fetch(`http://localhost:3000/api/todos/${obj.id}`, {
        method: 'DELETE'
    })
}

const response = await fetch('http://localhost:3000/api/todos');
    let todosList = await response.json();

export { saveDo };
export { changeDo };
export { deleteDo };
export { todosList };